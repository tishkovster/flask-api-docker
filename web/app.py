""" 
Registration of a user
Each user gets 10 tokens
Store a sentence on our database for 1 token
Retrieve his stored sentence on our database for 1 token
"""

from flask import Flask, jsonify, request
from flask_restful import Api, Resource
import bcrypt
from pymongo import MongoClient

app = Flask(__name__)
api = Api(app)


client = MongoClient("mongodb://db:27017")
db = client.SentencesDatabase


users = db["Users"]


def verifyPw(username, password):
    hashed_pw = users.find({
        "Username": username,

    })[0]['Password']
    return bcrypt.hashpw(password.encode("utf-8"), hashed_pw) == hashed_pw
    #     return True
    # else:
    #     return False


def countTokens(username):
    return users.find({"Username": username})[0]['Tokens']


class Register(Resource):

    def post(self):
        # Step 1 is to get posted data by  the user
        postedData = request.get_json()

        # GEt the data
        username = postedData['username']
        password = postedData['password']

        hashed_pw = bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt())
        # hash(password + salt) = FDFKASKREEfksfdks
        users.insert({
            "Username": username,
            "Password": hashed_pw,
            "Sentence": "",
            "Tokens": 6
        })
        retJson = {
            "status": 200,
            "msg": "Login successfull"
        }
        return jsonify(retJson)


class Store(Resource):
    def post(self):
        # Step 1- get the posted data
        postedData = request.get_json()
        # Read dem data
        username = postedData['username']
        password = postedData['password']
        sentence = postedData['sentence']

        # Step 3 verify the username pw match
        correct_password = verifyPw(username, password)
        if not correct_password:
            retJson = {
                "status": 403


            }
            return jsonify(retJson)
        # Step 4 Verify user has enough tokens
        num_tokens = countTokens(username)
        if num_tokens <= 0:
            retJson = {
                "status": 301
            }
            return jsonify(retJson)
        # Step 5 Store sencence and return 200
        users.update({
            "username": username
        }, {
            "$set": {
                "Sentence": sentence,
                "Tokens": num_tokens - 1
            }
        })

        retJson = {
            "status": 200,
            "msg": "Sentence saved"
        }
        return jsonify(retJson)


class Get(Resource):
    def get(self):
        postedData = request.get_json()

        username = postedData["username"]
        password = postedData["password"]
        orrect_password = verifyPw(username, password)
        if not correct_password:
            retJson = {
                "status": 403


            }
            return jsonify(retJson)
        if num_tokens <= 0:
            retJson = {
                "status": 301
            }
            return jsonify(retJson)

        sentence = users.find({
            "username": username
        })[0]['Sentence']

        retJson = {
            "status": 200,
            "sentence": sentence
        }

        return jsonify(retJson)


api.add_resource(Register, '/register')
api.add_resource(Store, '/store')


if __name__ == "__main__":
    # app.run(debug=True)
    app.run(host='0.0.0.0')


"""
UserNum.insert({
    'num_of_users': 0
})


class Visit(Resource):
    def get(self):
        prev_num = UserNum.find({})[0]['num_of_users']
        num_num = prev_num + 1
        UserNum.update({}, {"$set": {"num_of_users": num_num}})
        return str(f"Hello user {num_num}")


def checkPostedData(postedData, functionName):
    if "x" not in postedData or "y" not in postedData:
        return 301
    elif functionName == "divide" and postedData["y"] == 0:
        return 302
    else:
        return 200


class Add(Resource):
    def post(self):
        # Get posted data
        postedData = request.get_json()

        status_code = checkPostedData(postedData, "add")
        if status_code != 200:
            retJson = {"Message": "No x or y provided",
                       "Status Code": status_code
                       }
            return jsonify(retJson)
        x = postedData['x']
        y = postedData['y']
        x = int(x)
        y = int(y)
        ret = x+y
        retMap = {
            "Message": ret,
            "Status Code": 200
        }
        return jsonify(retMap)


class Substract(Resource):
            # Get posted data
    def post(self):
        postedData = request.get_json()

        status_code = checkPostedData(postedData, "subtract")
        if status_code != 200:
            retJson = {"Message": "No x or y provided",
                       "Status Code": status_code
                       }
            return jsonify(retJson)
        x = postedData['x']
        y = postedData['y']
        x = int(x)
        y = int(y)
        ret = x - y
        retMap = {
            "Message": ret,
            "Status Code": 200
        }
        return jsonify(retMap)


class Multiply(Resource):
            # Get posted data
    def post(self):
        postedData = request.get_json()

        status_code = checkPostedData(postedData, "multiply")
        if status_code != 200:
            retJson = {"Message": "No x or y provided",
                       "Status Code": status_code
                       }
            return jsonify(retJson)
        x = postedData['x']
        y = postedData['y']
        x = int(x)
        y = int(y)
        ret = x * y
        retMap = {
            "Message": ret,
            "Status Code": 200
        }
        return jsonify(retMap)


class Divide(Resource):
    def post(self):
        postedData = request.get_json()

        status_code = checkPostedData(postedData, "divide")
        if status_code != 200:
            retJson = {"Message": "No x or y provided",
                       "Status Code": status_code
                       }
            return jsonify(retJson)
        x = postedData['x']
        y = postedData['y']
        x = int(x)
        y = int(y)
        ret = (x*1.0)/y
        retMap = {
            "Message": ret,
            "Status Code": 200
        }
        return jsonify(retMap)


api.add_resource(Add, "/add")
api.add_resource(Substract, "/subtract")
api.add_resource(Multiply, "/multiply")
api.add_resource(Divide, "/divide")
api.add_resource(Visit, "/hello")


@ app.route('/')
def hello_world():
    return "Hello World!"


@ app.route("/hithere")
def hi_there_everyone():
    return "I just hit /hithere"


@ app.route("/add_two_nums", methods=["POST"])
def add_two_nums():
    # Get x, y from the posted date
    # Add z=x+y
    dataDict = request.get_json()
    x = dataDict['x']
    y = dataDict['y']
    z = x + y
    retJson = {'z': z}
    return jsonify(retJson), 200

    # Prepare a JSON, "z":z
    # return jsonify(map_prepared)


@ app.route('/bye')
def bye():
    c = 2*534
    # s = 1/0
    retJson = {
        'Name': 'Alex',
        'Age': 41,
        "phones": [
            {"phoneName": "Iphone",
             "phoneNumber": 1111
             },

            {"phoneName": "Xiaomi",
             "phoneNumber": 2222
             }
        ]
    }
    return jsonify(retJson)


if __name__ == "__main__":
    # app.run(debug=True)
    app.run(host='0.0.0.0')
"""
